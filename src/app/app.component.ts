import { Component } from '@angular/core';
import { ENVIRONMENT } from 'src/environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    version = ENVIRONMENT.version;
}
